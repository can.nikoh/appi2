import {configureStore} from '@reduxjs/toolkit'
import cartReducer from '../features/cart/indes'
import paymentReducer from '../features/Cobro/index'


const store = configureStore ({
    reducer:{
        cart:cartReducer,
        payment: paymentReducer
    }
})

export default store