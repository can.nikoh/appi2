import{ getApps, initializeApp, getApp} from 'firebase/app'
import{getFirestore} from 'firebase/firestore'
let app


if(!getApps.length){
    app =initializeApp({
        apiKey: process.env.FIREBASE_PUBLIC_API_KEY,
        authDomain: process.env.FIREBASE_AUTH_DOMAIN,
        projectId: process.env.FIREBASE_PROJECT_ID,
    
    })
}else{
    app = getApp()
}

// if(!admin.apps.length){
//      app = admin.initializeApp({
//         credential: admin.credential.cert(serviceAccount)
//     })
    
// }else{
//     app = admin.app()
// }

   const  database = getFirestore(app)


export default  database