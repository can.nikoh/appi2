import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { getCart } from '../features/cart/indes';
import Nabvar from './Nabvar';

export default function Page({children}) {

  const dispatch = useDispatch()
  
  useEffect(()=>{
    dispatch(getCart())
  },[])

  return <div className='bg-gray-100'>
      <Nabvar></Nabvar>
        <div className='max-w-4xl mx-auto px-5 '>
            {children}
        </div>;
  </div>
  
}
