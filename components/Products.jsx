import React from 'react';
import Producto from './Producto';

export default function Products({products}) {
  return <section className='grid  grid-cols-3 gap-5'>
             {products.map((product, i) => <Producto product={product} key={i} />)}
        </section>

}
