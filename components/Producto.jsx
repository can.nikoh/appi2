import React, { useState } from 'react';
import { motion } from 'framer-motion';
import { useDispatch } from 'react-redux';
import { addToCart, reduceFromCart, removeFromCart, saveCart } from '../features/cart/indes';
import {FaCartArrowDown, FaCartPlus, FaTrash} from 'react-icons/fa'


export default function Producto({product}) {
 


  const dispatch = useDispatch()
  const [show, setshowmenu] = useState(false)

  const agregaralCarrito = ()=>{
        dispatch(addToCart(product))
        dispatch(saveCart())
        
  }
  const removerDelCarrito =()=>{
        dispatch(removeFromCart(product))
        dispatch(saveCart())
  }

  const reducerCartItem =()=>{
        dispatch(reduceFromCart(product))
        dispatch(saveCart())
  }

 return  <motion.article 
              initial={{
                opacity: 0
              }}
              transition={{
                duration: 2
              }}
              animate={{
                opacity: 1
              }}
          className='bg-white shadow-md' >
        <div className='p-2'>
            <h3 className='text-2xl font-bold'>{product.name}</h3>
            <p className='my-3'>{product.description}</p>
            <p className='my-3'>{product.cantidad}</p>
            <button onClick={agregaralCarrito}><FaCartPlus className='text-sky-500 w-5 h-5 mr-5'/></button>
            <button onClick={reducerCartItem}><FaCartArrowDown className='text-sky-500 w-5 h-5 mr-5'/></button>
            <button onClick={removerDelCarrito}><FaTrash className='text-red-500 w-5 h-5 mr-5'/></button>
           
        </div>
        
        <img src={product.img}/>
  </motion.article>
}
