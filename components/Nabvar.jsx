import Link from 'next/link';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';

export default function Nabvar() {

  const {items:cart} = useSelector((state)=> state.cart)
  const tiempo = cart.length

  const [show, setshow] =useState(false)

  return <nav className='mx-auto bg-white shadow-md mb-5 px-5 py-3 sm:px-0'>
      <div className='flex   justify-between max-w-4xl mx-auto'>
      <li><Link href={"/"}> Inicio</Link></li>

      {
        console.log(cart)
      }
      <ul className={`flex gap-5 ${ show?"block": "hidden"} sm:flex`}>
          <li><Link href="/productos"> Productos</Link></li>
          <li><Link href="/productos"> Productos</Link></li>
          <li><Link href="/Cart"><p>Carrito {cart.length}</p></Link></li> 
      </ul>

      <button className='sm:hidden' onClick={()=>{setShow(!show)}}>Menu</button>

      </div>
       

  </nav>;
}
