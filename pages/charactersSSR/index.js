import Link from 'next/link';
import React from 'react';
import Head from 'next/head';
import Image from 'next/image';

export async function getServerSideProps(){
    const res = await fetch("https://rickandmortyapi.com/api/character")
  const characteres = await res.json()

  return {
    props:{
      characteres: characteres.results
    }
  }
}

export default function index({characteres}) {
    return <div>
      <Head>
        <title>Todos los personjes</title>
      </Head>
    {characteres.map(character=> <Link key={character.id} href={"charactersSSR/"+character.id}>
      <div>
        <p>{character.name}</p>
        <Image width={200} height={150} src={character.image}/>
      
      </div>
      </Link>)}
    </div>
}
