import axios from 'axios';
import React from 'react';
import Page from '../../components/Page';
import Products from '../../components/Products';

export async function getServerSideProps({req}){
    
    const {data:products} = await  axios.get(`http://${req.headers.host}/api/productos`)
          
    
  return {
    props:{
      products: products
    }
  }
}


export default function index({products}) {
  return <>
            <main className='p-5'>
              <Products products={products}/>
            </main>
        </>
   
  
}
