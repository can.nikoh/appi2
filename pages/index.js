import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link'
import { useState } from 'react'
import styles from '../styles/Home.module.css'
import axios from 'axios'
import Products from '../components/Products'
import Page from '../components/Page'

export async function getServerSideProps({req}){
  console.log(req.headers.host)
  const {data:products} = await  axios.get(`http://${req.headers.host}/api/productos/filtrar?popular=true`)
        
  
return {
  props:{
    products: products
  }
}
}


export default function Home({products}) {

  const [open, setOpen]= useState(false)

  return (
    
        <>
            <p className='text-yellow-400 text-3xl'>Hol con Estiolos de Tailwind</p>
            <button onClick={()=>{setOpen(!open)}}>Presionar</button>
            { open&&<p>Hola!</p>}
          <Link href={"/characters"}>Ir a Personajes</Link>
          <Link href={"/charactersSSR"}>Ir a Personajes SRR</Link>

            <Products products={products} />
        </>
    
    

    )
}
