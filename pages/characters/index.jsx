import Link from "next/link"
import Image from "next/image"
import Head from "next/head"

export async function getStaticProps(){
  const res = await fetch("https://rickandmortyapi.com/api/character")
  const characteres = await res.json()

  return {
    props:{
      characteres: characteres.results
    }
  }
}


export default function index() {
  return <div>
    <Head>
        <title>Todos los personjes</title>
      </Head>
    
  </div>
}
