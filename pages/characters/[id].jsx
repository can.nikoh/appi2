import { useRouter } from 'next/router';
import React from 'react';

export async function getStaticPaths() {
    const res = await fetch("https://rickandmortyapi.com/api/character")
    const { results:characteres }= await res.json()

    const rutas = characteres.map(character=>({
      params:{
        id:""+character.id
      }
    }))

    console.log(rutas)
    return {
      paths: rutas,
      fallback: false
    }
}

export async function getStaticProps({params}){
  const res = await fetch("https://rickandmortyapi.com/api/character/"+params.id)
  const character = await res.json()

  return {
    props:{
      character
    }
  }
}

export default function Usuario({character}) {
  const router = useRouter()
  

  return <div><h1>Personaje: {character.name}</h1></div>;
}
