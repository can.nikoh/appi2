import database from "../../../database"
import { collection, getDocs, query, where } from 'firebase/firestore'


export default async function filtrar(req, res){
    const {popular} = req.query

    //consulta informacion 

    const col = collection(database, "productos")
    let snapshot

    if(popular){
        const consulta = query(col, where('popular', '==', true))
        snapshot = await getDocs(consulta)
    }else{
        const consulta = query(col)
        snapshot = await getDocs(consulta)
    }



    if(snapshot.empty){
        return res.status(404).json({message:"No se encontraron documentos"})
    }

    const productos = []

    snapshot.forEach(doc=>{
        productos.push({id:doc.id, ...doc.data()})
    })

    return res.status(200).json(productos)
}

//CON FARIBASE ADMIN
// export default async function filtrar(req, res){

//     const {popular} = req.query

    
//     const colection =  database.collection("productos")
//     let snapshot

//     if(popular){ 
//         snapshot  =  await colection.where('popular','==',true).get()
//     }else{
//         snapshot =  await colection.get()
//     }

    
    
    
//     if(snapshot.empty){
//         return res(404).json({message: "Documento no encontrado"})
//     }

//     const productos = []
//     snapshot.forEach(doc => {
//             productos.push({id:doc.id, ...doc.data()})
//     })

    

//     return res.status(200).json(productos)

    
// }